package com.example.pushserver;

import static java.util.Optional.ofNullable;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@RestController
@Slf4j
public class WebPushServerApplication {

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public static class BadRequestException extends RuntimeException {
		public BadRequestException(String msg, Throwable t) {
			super(msg, t);
		}

		public BadRequestException(String msg) {
			super(msg);
		}
	}

	@Data
	public static class WebPushSubscription {

		private String token;
		private String id;
	}

	@Data
	public static class SendPushRequest {
		private String id;
		private String title;
		private String message;
	}

	private static Map<String, WebPushSubscription> subscriptions = new ConcurrentHashMap<>();

	public static void main(String[] args)
			throws NoSuchProviderException, NoSuchAlgorithmException,
			InvalidKeySpecException {

		SpringApplication.run(WebPushServerApplication.class, args);
	}

	@PostMapping("/subscribe")
	public void subscribe(@RequestBody WebPushSubscription subRequest) {
		log.info("subscribe received: {}", subRequest);
		subscriptions.put(subRequest.id, subRequest);
	}

	@PostMapping("/unsubscribe")
	public void unsubscribe(@RequestBody WebPushSubscription subscription) {
		subscriptions.remove(subscription.id);
	}

	@PostMapping("/notify")
	public void notifyDevice(@RequestBody SendPushRequest req) {
		final var deviceId = ofNullable(req.id)
				.orElseThrow(() -> new BadRequestException("ID do dispositivo não pode ser nulo."));
		final var subscription = ofNullable(subscriptions.get(deviceId))
				.orElseThrow(() -> new BadRequestException("Dispositivo não cadastrado."));
		final var title = ofNullable(req.getTitle()).orElse("From server");

		final var message = Message.builder()
				.setNotification(Notification.builder().setTitle(title).setBody(req.getMessage()).build())
				.setToken(subscription.getToken())
				.build();

		try {
			final var response = FirebaseMessaging.getInstance().send(message);
			log.info("Mensagem push enviada ao dispositivo. [id={}]", deviceId);
		} catch (FirebaseMessagingException e) {
			log.error("Erro ao enviar mensagem ao Firebase.", e);
			throw new RuntimeException(e);
		}
	}

	@GetMapping("/list")
	public List<String> listSubscribers() {
		return subscriptions.values().stream().map(sub -> sub.getId()).collect(Collectors.toList());
	}
}
