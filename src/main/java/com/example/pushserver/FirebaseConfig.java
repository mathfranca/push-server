package com.example.pushserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class FirebaseConfig {

	@Bean
	public FirebaseMessaging firebaseMessaging(FirebaseApp firebaseApp) {
		log.info("Setting up Firebase messaging");
		return FirebaseMessaging.getInstance(firebaseApp);
	}

	@Bean
	public FirebaseApp firebaseApp() throws IOException {
		log.info("Setting up Firebase connection");
		try {
			final var classLoader = getClass().getClassLoader();
			final var file = new File(classLoader.getResource("firebasekey.json").getFile());
			final var serviceAccount = new FileInputStream(file);

			final var options = FirebaseOptions.builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.build();

			return FirebaseApp.initializeApp(options);
		} catch (Exception e) {
			log.error("Failed to setup Firebase connection: {}", e.getMessage());
			throw e;
		}
	}
}
